//
//  ProfileManager.swift
//  Test application Eureka
//
//  Created by Mikhail Kolesov on 28.03.2023.
//

import Foundation

class ProfileRepository {
    private let profileUDKey = "eureka.test.profile.key"
    
    var profile: Profile {
        get {
            let defaultValue = Profile(
                name: nil,
                avatarString: nil,
                age: nil,
                genderString: nil,
                dateOfBirth: nil
            )
            guard let data = UserDefaults.standard.data(forKey: profileUDKey) else { return
                defaultValue
            }
            let decodedObject = try? JSONDecoder().decode(Profile.self, from: data)
            return decodedObject ?? defaultValue
        } set {
            if let newData = try? JSONEncoder().encode(newValue) {
                UserDefaults.standard.set(newData, forKey: profileUDKey)
            } else {
                UserDefaults.standard.removeObject(forKey: profileUDKey)
            }
        }
    }
}
