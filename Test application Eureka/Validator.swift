//
//  Validator.swift
//  Test application Eureka
//
//  Created by Mikhail Kolesov on 28.03.2023.
//

import Foundation
import Eureka

class Validator {
    func validate(age: String) -> ValidationError? {
        if let int = Int(age), int > 0, int < 100 {
            return nil
        } else {
            return ValidationError(msg: "Please write only numbers!")
        }
    }
    
    func validate(name: String) -> ValidationError? {
        let numbersRange = name.rangeOfCharacter(from: .decimalDigits)
        let punctuationRange = name.rangeOfCharacter(from: .punctuationCharacters)
        let hasPunctuation = (punctuationRange != nil)
        let hasNumbers = (numbersRange != nil)
        if !hasNumbers && !hasPunctuation {
            return nil
        } else {
            return ValidationError(msg: "Please write only letters!")
        }
    }
}
