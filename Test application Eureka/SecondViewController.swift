//
//  SecondViewController.swift
//  Test application Eureka
//
//  Created by Mikhail Kolesov on 16.03.2023.
//

import UIKit
import Eureka
import ImageRow

class SecondViewController: FormViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCells()
    }
    
    func configureCells() {
        form +++
        MultivaluedSection(
            multivaluedOptions: [.Reorder, .Insert, .Delete],
            header: "Form",
            footer: "Insert adds a 'Add Item' (Add New Tag) button row as last cell"
        ) {
            $0.addButtonProvider = { section in
                return ButtonRow() {
                    $0.title = "Add New Tag"
                }
            }
            $0.multivaluedRowToInsertAt = { index in
                return NameRow() {
                    $0.placeholder = "Tag Name"
                }
            }
            $0 <<< NameRow() {
                $0.placeholder = "Tag Name"
            }
        }
    }
}
