//
//  Profile.swift
//  Test application Eureka
//
//  Created by Mikhail Kolesov on 27.03.2023.
//

import UIKit

struct Profile: Codable {
    var name: String?
    var avatarString: String?
    var age: String?
    var genderString: String?
    var dateOfBirth: Date?
    
    enum CodingKeys: String, CodingKey {
        case name
        case avatarString
        case age
        case genderString
        case dateOfBirth
    }
}

enum Gender: String {
    case male = "Male"
    case female = "Female"
    case undefined = "not selected"
}
