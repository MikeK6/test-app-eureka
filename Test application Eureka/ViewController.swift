//
//  ViewController.swift
//  Test application Eureka
//
//  Created by Mikhail Kolesov on 16.03.2023.
//

import UIKit
import Eureka
import ImageRow

class ViewController: FormViewController {
    
    // MARK: - Properties
    
    private var profile: Profile {
        get {
            ProfileRepository().profile
        }
        set {
            ProfileRepository().profile = newValue
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createCells()
    }
    
    // MARK: - Methods
    
    private func createCells() {
        let section = Section("Personal info")
        form +++ section
        
        section <<< ImageRow {
            $0.title = "Photo"
            $0.selectorTitle = "Take a photo"
            $0.sourceTypes = [.PhotoLibrary, .SavedPhotosAlbum, .Camera]
            $0.clearAction = .yes(style: .default)
            $0.allowEditor = true
            $0.useEditedImage = true
            $0.value = profile.avatarString?.toImage()
        }.onChange({ row in
            self.profile.avatarString = row.value?.toJpegString()
        })
        
        section <<< TextRow {
            $0.title = "Name"
            $0.placeholder = "Write name"
            $0.value = profile.name
            $0.add(rule: RuleClosure(closure: { text in
                Validator().validate(name: text ?? "")
            }))
        }.onChange { row in
            row.validate()
            if row.isValid {
                self.profile.name = row.value
            }
        }.onRowValidationChanged { cell, row in
            self.onValidationChanged(cell: cell, row: row)
        }
        
        section <<< ActionSheetRow<String> {
            $0.title = "Gender"
            $0.noValueDisplayText = "Pick a gender"
            $0.options = [Gender.male.rawValue, Gender.female.rawValue, Gender.undefined.rawValue]
            $0.value = profile.genderString
        }.onChange({ row in
            self.profile.genderString = row.value
        })
        
        section <<< TextRow {
            $0.title = "Age"
            $0.placeholder = "Enter age"
            $0.value = profile.age
            $0.add(rule: RuleClosure(closure: { text in
                Validator().validate(age: text ?? "")
            }))
        }.onChange { row in
            row.validate()
            if row.isValid {
                self.profile.age = row.value
            }
        }.onRowValidationChanged { cell, row in
            self.onValidationChanged(cell: cell, row: row)
        }
        
        section <<< DateRow {
            $0.title = "Date of birth"
            $0.noValueDisplayText = "select the date"
            $0.value = profile.dateOfBirth
        }.onChange({ [weak self] row in
            self?.profile.dateOfBirth = row.value
        })
        
        let secondSection = Section("Settings")
        form +++ secondSection

        secondSection <<< ButtonRow {
            $0.title = "Go to Settings"
            $0.presentationMode = .segueName(segueName: "Second", onDismiss: nil)
        }
    }
    
    private func onValidationChanged(cell: TextCell, row: TextRow) {
        guard let indexPath = row.indexPath else { return }
        let rowIndex = indexPath.row
        while (row.section?.count ?? .zero) > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
            row.section?.remove(at: rowIndex + 1)
        }
        
        if !row.isValid {
            for (index, validationMsg) in row.validationErrors.map({ $0.msg }).enumerated() {
                let labelRow = LabelRow() {
                    $0.title = validationMsg
                    $0.cell.height = { 30 }
                    $0.cell.backgroundColor = .systemRed
                    $0.cell.detailTextLabel?.textColor = .white
                }
                let indexPath = indexPath.row + index + 1
                row.section?.insert(labelRow, at: indexPath)
            }
        }
    }
}
