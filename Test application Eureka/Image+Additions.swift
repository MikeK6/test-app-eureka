//
//  Image+Additions.swift
//  Test application Eureka
//
//  Created by Mikhail Kolesov on 28.03.2023.
//

import UIKit

extension String {
    func toImage() -> UIImage? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) {
            return UIImage(data: data)
        }
        return nil
    }
}
extension UIImage {
    func toJpegString(compressionQuality cq: CGFloat = 0.9) -> String? {
        let data = self.jpegData(compressionQuality: cq)
        return data?.base64EncodedString(options: .endLineWithLineFeed)
    }
}

